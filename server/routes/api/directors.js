const express = require("express");
const router = express.Router();
const connection = require("../../dbConnect");

// Gets All Members
router.get("/", (req, res) => {
  connection.query("select * from Directors", (error, results, fields) => {
    res.send(results);
  });
});
// Add a new director
router.post("/", (req, res) => {
  let query = `insert into Directors (D_name) value ("${req.headers.name}")`;
  connection.query(query, (error, results, fields) => {
    if (error) throw error;
    res.send("Director name inserted");
  });
});

// Get the director with given ID
router.get("/:id", (req, res) => {
  connection.query(
    `select * from Directors where id = "${req.params.id}`,
    (error, results, fields) => {
      if (results == undefined) {
        res.send("Entered Director id is not present");
      } else {
        let query = `select * from Directors where d_id = ${req.params.id}`;
        connection.query(query, (error, results, fields) => {
          res.send(results);
        });
      }
    }
  );
});
//  to update
router.put("/:id", (req, res) => {
  connection.query(
    `select * from Directors where id = "${req.params.id}`,
    (error, results, fields) => {
      if (results == undefined) {
        res.send("Entered Director id is not present");
      } else {
        let query = `UPDATE Directors
      SET D_name = '${req.query.name}'
      WHERE d_id = '${req.params.id}'`;
        connection.query(query, (error, results, fields) => {
          if (error) throw error;
          res.send("Director name updated");
        });
      }
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `select * from Directors where id = "${req.params.id}`,
    (error, results, fields) => {
      if (results == undefined) {
        res.send("Entered Director id is not present");
      } else {
        let query = `delete from Directors where d_id = "${req.params.id}"`;
        // console.log(req);
        connection.query(query, (error, results, fields) => {
          if (error) throw error;
          res.send("Director Deleted");
        });
      }
    }
  );
});
module.exports = router;

// connection.query(
//   `select * from Directors where id = "${req.params.id}`,
//   (error, results, fields) => {
//     if(results == undefined){
//       res.send("Entered Director id is not present")
//     }else{

//     }
//   })
