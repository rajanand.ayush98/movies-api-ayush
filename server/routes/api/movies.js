const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");
const filepath = path.resolve(__dirname + "/../../movies.json");

const connection = require("../../dbConnect");

router.get("/", (req, res) => {
  connection.query(
    "Select Title from Movies_Register",
    (error, results, fields) => {
      res.send(results);
    }
  );
});

router.post("/", (req, res) => {
  let query = `insert into Movies_Register 
    (Title, Description, Runtime, Genre, Rating, MetaScore, Votes, Gross_Earning_in_Mil, d_id, Actor, Year) 
    value ("${req.query.Title}","${req.query.Description}",
    "${req.query.Runtime}","${req.query.Genre}",
    "${req.query.Rating}","${req.query.MetaScore}",
    "${req.query.Votes}",
    "${req.query.Gross_Earning_in_Mil}","${req.query.d_id}",
    "${req.query.Actor}","${req.query.Year}")`;
  connection.query(query, (error, results, fields) => {
    if (error) throw error;
    res.send(`Movie values inserted with values "${req.query.Title}","${req.query.Description}",
    "${req.query.Runtime}","${req.query.Genre}",
    "${req.query.Rating}","${req.query.MetaScore}",
    "${req.query.Votes}",
    "${req.query.Gross_Earning_in_Mil}","${req.query.d_id}",
    "${req.query.Actor}","${req.query.Year}" `);
  });
});

// to get a movie
router.get("/:id", (req, res) => {
  //   console.log(idFilter(req));
  let query = `select * from Movies_Register where id = ${req.params.id}`;
  connection.query(query, (error, results, fields) => {
    res.send(results);
  });
});

// to update a movie
router.put("/:id", (req, res) => {
  let query = `UPDATE Movies_Register
    SET Title = '${req.query.Title}',
    Description = '${req.query.Description}',
    Runtime = '${req.query.Runtime}',
    Genre = '${req.query.Genre}',
    Rating = '${req.query.Rating}',
    MetaScore = '${req.query.MetaScore}',
    Votes = '${req.query.Votes}',
    Gross_Earning_in_Mil = '${req.query.Gross_Earning_in_Mil}',
    d_id = '${req.query.d_id}',
    Actor = '${req.query.Actor}',
    Year = '${req.query.Year}'
    
    WHERE id = ${req.params.id}`;
  connection.query(query, (error, results, fields) => {
    if (error) throw error;
    res.send("Director name updated");
  });
});

// to delete a movie
router.delete("/:id", (req, res) => {
  connection.query(
    `select * from Movies_Register where id = "${req.params.id}`,
    (error, results, fields) => {
      if (results == undefined) {
        res.send("Entered Id is not present");
      } else {
        let query = `delete from Movies_Register where id = "${req.params.id}"`;
        connection.query(query, (error, results, fields) => {
          if (error) throw error;
          res.send("Movie Deleted");
        });
      }
    }
  );
});
module.exports = router;
