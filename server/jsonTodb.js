const mysql = require("mysql");
const fs = require("fs");
const connection = require("./dbConnect");
const path = require("path");
const filepath = path.resolve(__dirname + "/movies.json");
const jsonFileMatches = JSON.parse(fs.readFileSync(filepath));
let directorObj = {};
// console.log(jsonFileMatches);
exports.insertToDb = () => {
  connection.query(
    "create table if not exists Directors(d_id int Primary key Auto_Increment, D_name text)",
    (error, results, fields) => {
      if (error) throw error;
      if (results.warningCount == 0) {
        let id = 1;
        jsonFileMatches.forEach((element) => {
          if (!directorObj[element.Director]) {
            directorObj[element.Director] = id;
            id += 1;
          }
        });

        let directorArr = Object.keys(directorObj);

        directorArr.forEach((values) => {
          let query =
            "insert into Directors (D_name ) values(" +
            "'" +
            values +
            "'" +
            ")";
          connection.query(query, (error, results, fields) => {
            if (error) throw error;
          });
        });
      }
    }
  );

  connection.query(
    "create table if not exists Movies_Register(id int PRIMARY KEY Auto_Increment, Title text, Description text, Runtime int, Genre text, Rating text, MetaScore text, Votes int,\
    Gross_Earning_in_Mil text, d_id int, Actor text, Year int, foreign key (d_id) references Directors(d_id) )",
    (error, results, fields) => {
      if (error) throw error;
      if (results.warningCount == 0) {
        jsonFileMatches.forEach((values) => {
          connection.query(
            `select d_id as id from Directors where D_name = '${values.Director}'`,
            (error, results, fields) => {
              if (error) throw error;
              values.Director = results[0].id;
              values = Object.values(values);
              values = values.map((element) => `"${element}"`);
              let query = "insert into Movies_Register values(" + values + ")";
              c;
              connection.query(query, (error, results, fields) => {
                if (error) throw error;
              });
            }
          );
        });
      }
    }
  );
};
