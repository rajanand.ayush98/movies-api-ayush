const mysql = require("mysql");
const fs = require("fs");
const envVariables = require("./config.js");
const path = require("path");
const express = require("express");
const connection = require("./dbConnect");
const db = require("./jsonTodb");
const app = express();
const filepath = path.resolve(__dirname + "/movies.json");

connection;
db.insertToDb();
app.get("/", (req, res) => {
  res.send("Movies Api Project");
});
app.use("/api/directors", require("./routes/api/directors"));
app.use("/api/movies", require("./routes/api/movies"));
const PORT = process.env.PORT || envVariables.port;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
