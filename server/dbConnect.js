const mysql = require("mysql");
const fs = require("fs");
const envVariables = require("./config.js");
const path = require("path");
const express = require("express");

const connection = mysql.createConnection({
  host: envVariables.host,
  database: envVariables.database,
  user: envVariables.user,
  password: envVariables.password,
});

connection.connect((err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("connection established!");
  }
});

console.log("yes it works!");

module.exports = connection;
